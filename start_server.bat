@echo off
color 17
if not defined VIRTUAL_ENV (
  echo Virtual environment not activated, activating...
  call venv/Scripts/activate.bat
)
set FLASK_APP=main.py
if "%1"=="debug" (
    echo Setting debug mode flag
    set FLASK_DEBUG=1
) else (
    echo Disabling debug mode flag
    set FLASK_DEBUG=0
)
echo.
py -m flask run
