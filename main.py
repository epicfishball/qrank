import os
import sqlite3

from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash

#import qrank
from .qrank import Question

app = Flask(__name__)
app.config.from_object(__name__)
app.config.update(dict(
    DATABASE = os.path.join(app.root_path, 'database', 'questionbank.db'),
    SECRET_KEY = 'fishball123', # secures client sessions; to use proper token
    USERNAME = 'admin',
    PASSWORD = 'password123'
))
# Uncomment below to load a separate, environment-specific configuration file
# You will have to set FLASK_APP=some_cfg_file
#app.config.from_envvar('FLASK_APP', silent=True)


#
# Generic helper functions
#
def connect_db():
    """Connects to the app database.

    Allows us create a connection to the database from py -i or from a script.
    """
    rv = sqlite3.connect(app.config['DATABASE'])
    # Use the sqlite3.Row object to represent rows, which allows them to be 
    # treated as dictionaries instead of tuples:
    rv.row_factory = sqlite3.Row 
    return rv


def get_db():
    """Opens a new db conn for the app context if there isn't one yet."""
    if not hasattr(g, 'questionbank_db'):
        g.questionbank_db = connect_db()
    return g.questionbank_db

@app.teardown_appcontext
def close_db(error):
    """Closes the database at the end of the request (when the app context is
    closed)."""
    if hasattr(g, 'questionbank_db'):
        g.questionbank_db.close()


def init_db():
    """Initialises the database with a schema."""
    db = get_db()
    schema_path = os.path.join('database', 'schema.sql')
    with app.open_resource(schema_path, mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()
    print('Initialized the database.')


@app.cli.command('initdb')
def initdb_command():
    """CLI command to initialise the database."""
    init_db()



#
# Specific query helpers
#
def get_questions():
    db = get_db()
    query = 'select * from questionbank order by id asc'
    cur = db.execute(query)
    entries = cur.fetchall()
    return [Question(x) for x in entries]



#
# App http endpoints
#
@app.route('/')
def main_page():
    return render_template('index.html')


@app.route('/questions')
def view_questions():
    return render_template('questions.html', questions=get_questions())


