drop table if exists questionbank;
create table questionbank (
  id integer primary key autoincrement,
  question text not null,
  answer text not null,
  distractor1 text,
  distractor2 text,
  distractor3 text,
  distractor4 text,
  distractor5 text,
  distractor6 text, 
  distractor7 text
);

insert into questionbank(
    question, 
    answer, 
    distractor1, 
    distractor2, 
    distractor3, 
    distractor4, 
    distractor5, 
    distractor6, 
    distractor7
) values (
    'Which of these words is not of French origin?', 
    'Heathen', 
    'Beef', 
    'Lieutenant', 
    'Parliament',
    NULL,
    NULL,
    NULL,
    NULL
), (
    'Which of the following is/are not a builtin type(s) in Python?',
    'array',
    'dictionary',
    'boolean',
    'string',
    'number',
    'list',
    NULL,
    NULL
);