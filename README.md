# Qrank Webapp

## Setting up the app
1. `git clone` this repo
2. Use command prompt.
3. Create a virtual environment:  
   `md venv`
   `cd venv`  
   `py -m venv %cd%` - this creates a virtual environment in the "venv" folder.
4. `cd` into `venv\Scripts\` and run `activate.bat` - this activates the virtual environment.
   Your cmd prompt should change to include `(venv)` in front.
5. `pip install flask` - this installs Flask within the virtual env ("venv" for short).
6. `cd ..` a bunch of times to get to the folder containing start_server.bat
   At this point you should have made a `venv` folder and created a virtual environment in it.
   You should have also installed `flask` in the virtual environment.
   You don't need to have the venv activated before running `start_server.bat`.
   (While you're in the venv, you can deactivate it by typing `deactivate`)
7. `start_server.bat` (for debugging, `start_server.bat debug` instead)
8. On your browser, go to `localhost:5000`

## Modifying stuff
- Webapp-related stuff is in `main.py`.
- Core-related stuff is in `qrank.py` (in the app's current stage, it's currently unused and outdated).
- To add questions, modify database/schema.sql, then with venv activated, run `flask initdb`.

## In the pipeline
- Implement `Player` objects so we can have a baseline to start using Elo with.
- Method to insert questions without re-generating the database.