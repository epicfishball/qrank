from random import shuffle

REPR_QUESTION_LENGTH = 15

class Empty:
    pass

class Question:
    """Unpacks a db row into a script-friendly object."""
    def __init__(self, sqlite_obj):
        self.id =  sqlite_obj['id']
        self.question = sqlite_obj['question']
        self.answer = sqlite_obj['answer']

        # 
        distrc_keys = ['distractor'+str(i) for i in range(1, 7+1)]
        self.distractors = list(filter(
            lambda x: x,
            [sqlite_obj[key] for key in distrc_keys]
        ))
        
        self.num_choices = len(self.distractors) + 1
    
    def __repr__(self):
        i = self.id
        q = self.question[:REPR_QUESTION_LENGTH - 3] + '...'
        if len(q) <= REPR_QUESTION_LENGTH:
            # Check <= since expecting most qns to need truncation
            q = self.question
        n = self.num_choices
        return '<Question: {} - {} ({} choices)>'.format(i, q, n)

    @property
    def jumbled_options(self):
        options = list.copy(self.distractors)
        shuffle(options)
        return options
